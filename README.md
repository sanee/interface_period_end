## 界面设计期末-许钰然、冯静雯、黄梓珊
<img src="https://images.gitee.com/uploads/images/2020/0705/120611_3500ea4d_2228445.png ">
<img src="https://images.gitee.com/uploads/images/2020/0701/144848_ecc98591_2233457.jpeg " weight="450px" height="1300px" >
<img src="https://images.gitee.com/uploads/images/2020/0701/144835_9db229ed_2233457.jpeg " weight="450px"  height="1300px">


### C. 产品页面的高保真原型图
# [原型](https://modao.cc/app/ae51a74f467be273cd0fa7363d4fa57844e66ce0?simulator_type=device&sticky)

#### 登录页面

<div align=center><img src="https://images.gitee.com/uploads/images/2020/0630/175104_6d6f08ed_2228445.png" height="600" ></div>

#### HOME

<div align=center><img src="https://images.gitee.com/uploads/images/2020/0630/175124_769a55b4_2228445.png" height="600" ></div>

#### 拍照瞬间

<div align=center><img src="https://images.gitee.com/uploads/images/2020/0630/175138_61fb1cde_2228445.png" height="600" ></div>

#### 花典 进行花草知识科普

<div align=center><img src="https://images.gitee.com/uploads/images/2020/0630/175153_03d81197_2228445.png" height="600" ></div>

#### 识别花草

<div align=center><img src="https://images.gitee.com/uploads/images/2020/0630/175206_e28c9ee2_2228445.png" height="600" ></div>

#### 识别花草的结果

<div align=center><img src="https://images.gitee.com/uploads/images/2020/0630/175225_12da9ea4_2228445.png" height="600" ></div>

#### 商城

<div align=center><img src="https://images.gitee.com/uploads/images/2020/0630/175238_86eeae4b_2228445.png" height="600" ></div>

#### 商品详情页面

<div align=center><img src="https://images.gitee.com/uploads/images/2020/0630/175253_c9a2b967_2228445.png" height="600" ></div>

#### 购物车

<div align=center><img src="https://images.gitee.com/uploads/images/2020/0630/175309_bd661b00_2228445.png" height="600" ></div>

#### 购物车管理

<div align=center><img src="https://images.gitee.com/uploads/images/2020/0630/175321_bef96f37_2228445.png" height="600" ></div>

#### 购物车无货物时候的状态

<div align=center><img src="https://images.gitee.com/uploads/images/2020/0630/175333_7ae0b0e8_2228445.png" height="600" ></div>

#### 个人信息及设置页面

<div align=center><img src="https://images.gitee.com/uploads/images/2020/0630/175344_a25bd61d_2228445.png" height="600" ></div>

#### 每日签到



<div align=center><img src="https://images.gitee.com/uploads/images/2020/0630/175359_d964aaf7_2228445.png" height="600" ></div>















